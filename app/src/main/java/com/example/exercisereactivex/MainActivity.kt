package com.example.exercisereactivex

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.AsyncSubject
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import io.reactivex.rxjava3.subjects.ReplaySubject
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Example Hot Observable vs Cold Observable
        Log.i("TAG", "From Just")
        val justSingle = Single.just(Random.nextInt())
        justSingle.subscribe { it -> Log.i("TAG", it.toString()) }
        justSingle.subscribe { it -> Log.i("TAG", it.toString()) }
        Log.i("TAG", "Callable Single")
        val callableSingle = Single.fromCallable { Random.nextInt() }
        callableSingle.subscribe { it -> Log.i("TAG", it.toString()) }
        callableSingle.subscribe { it -> Log.i("TAG", it.toString()) }
        //Some way create observable.
        val fromArray = Observable.fromArray(arrayOf("One", "Two"))
        fromArray.subscribe(object : Observer<Array<String>> {
            override fun onSubscribe(d: Disposable) {
                Log.i("TAG", "onSubcribe")
            }

            override fun onNext(t: Array<String>) {
                Log.i("TAG", t[0])
            }

            override fun onError(e: Throwable) {
                Log.i("TAG", e.toString())
            }

            override fun onComplete() {
                Log.i("TAG", "onComplete")
            }
        })
        fromArray.subscribe(object : Observer<Array<String>> {
            override fun onSubscribe(d: Disposable) {
                Log.i("TAG", "onSubcribe")
            }

            override fun onNext(t: Array<String>) {
                Log.i("TAG", t[1])
            }

            override fun onError(e: Throwable) {
                Log.i("TAG", e.toString())
            }

            override fun onComplete() {
                Log.i("TAG", "onComplete")
            }
        })
        //create/timer/interval/...

        //RxAndroid
        //Schedulers.io(),Schedulers.mainThread(),Schedulers.newThread,Schedulers.computation,Shedulers.trampoline.

        //Subject
        //AsynSubject ko phát ra giá trị bất kì nào cho đến khi nó được hoàn thành thì giá trị cuối cùng được phát ra.
        val s1 = AsyncSubject.create<Int>()
        s1.subscribe { i -> Log.i("TAG", i.toString()) }
        s1.onNext(0)
        s1.onNext(1)
        s1.onNext(2)
        s1.onComplete()
        //BehaviorSubject:Lưu giá trị cuối cùng  có ngay khi subcribe nó khác asynSubject là phải kết thúc.
        val s2 = BehaviorSubject.create<Int>()
        s2.onNext(0)
        s2.onNext(1)
        s2.onNext(2)
        s2.subscribe { i -> Log.i("TAG", i.toString()) }
        s2.onNext(3)
        //ReplaySubject cached lại các observable phát ra khi chưa subcribe.
        // Để giời hạn ta dùng createWithSize vs createWithTime.
        val s3 = ReplaySubject.create<Int>()
        s3.subscribe { i -> Log.i("TAG", i.toString()) }
        s3.onNext(0)
        s3.onNext(1)
        s3.subscribe { i -> Log.i("TAG", i.toString()) }
        s3.onNext(2)
        //PulishSubject:Nếu observable phát ra chưa subcribe thì có giá trị đó chưa nhận được.mà nó củng ko đưa vô cached.
        val s4 = PublishSubject.create<Int>()
        s4.onNext(1)
        s4.subscribe { i -> Log.i("TAG", i.toString()) }
        s4.onNext(2)
        s4.onNext(3)
        s4.onNext(4)
    }

    override fun onResume() {
        super.onResume()
    }
}
